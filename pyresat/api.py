# coding: utf-8

import os
import json
import shutil
import hashlib
import image

from flask import Flask, Response, send_from_directory, render_template, request
from flask_restful import Resource, Api
from zipfile import ZipFile, BadZipfile
from glob import glob
from StringIO import StringIO
from base64 import b64decode
from database import Database
from utils import create_folder
from neural import generate_dataset_from_images, net2


ROOT = os.path.dirname(__file__)
STORAGE_PATH = os.path.join(ROOT, "storage")

if not os.path.exists(STORAGE_PATH):
    create_folder(STORAGE_PATH)


app = Flask(__name__, static_url_path="/static")
api, db = Api(app), Database()


@app.route('/storage/<path:filename>')
def static_proxy(filename):
    return send_from_directory(STORAGE_PATH, filename)


class Uploader(Resource):

    @staticmethod
    def get():
        """..."""
        return Response(render_template("uploader.html"))

    @staticmethod
    def post():
        """..."""
        data = json.loads(request.data)

        model_dir = os.path.join(ROOT, data["path"])
        if not os.path.exists(model_dir):
            return {"error": {"code": 1, "message": "Wrong path."}}

        filename = os.path.join(model_dir, "rendered", "{}_{}_{}.png".format(data["X"], data["Y"], data["Z"]))
        if not os.path.exists(os.path.dirname(filename)):
            create_folder(model_dir, "rendered")

        with open(filename, "wb") as f:
            f.write(b64decode(data["image"][21:]))

        # Prepare image for using by neural network.
        image.process(filename)

        return {"success": {"code": 1, "message": "The file was uploaded successfully."}}


class Models(Resource):
    """List of available models."""

    @staticmethod
    def get():
        """Get a list of available models.

        Returns:
            list of available models.
        """
        return [model.to_dict() for model in db.list_models()]

    @staticmethod
    def post():
        """Create new model from uploaded file with corresponding name.

        Returns:
            ...
        """
        content = request.files["file"].read()

        # Create unique directory.
        model_dir = os.path.join(STORAGE_PATH, hashlib.md5(content).hexdigest())
        if os.path.exists(model_dir):
            return {
                "success": {
                    "code": 1,
                    "message": "The model is already added."
                },
                "path": "storage/{}/".format(os.path.split(model_dir)[-1]),
                "obj": os.path.basename(glob(os.path.join(model_dir, "*.obj"))[0]),
                "mtl": os.path.basename(glob(os.path.join(model_dir, "*.mtl"))[0]),
            }

        create_folder(folder=model_dir)

        # Try to extract archive to local dir.
        try:
            with ZipFile(StringIO(content)) as archive:
                archive.extractall(model_dir)
        except BadZipfile:
            shutil.rmtree(model_dir)
            return {"error": {"code": 1, "message": "This is not a zip archive."}}

        # Look for obj file. Should be exactly one.
        obj_files = glob(os.path.join(model_dir, "*.obj"))
        if not obj_files:
            shutil.rmtree(model_dir)
            return {"error": {"code": 2, "message": "There is no one .obj file in archive."}}
        elif len(obj_files) != 1:
            shutil.rmtree(model_dir)
            return {"error": {"code": 3, "message": "There is more than one .obj file in archive."}}

        # Finally, add this model to database.
        db.add_model(os.path.basename(obj_files[0]), model_dir)
        return {
            "success": {
                "code": 2,
                "message": "The model has been successfully added to the collection."
            },
            "path": "storage/{}/".format(os.path.split(model_dir)[-1]),
            "obj": os.path.basename(glob(os.path.join(model_dir, "*.obj"))[0]),
            "mtl": os.path.basename(glob(os.path.join(model_dir, "*.mtl"))[0]),
        }


class NeuralNetwork(Resource):
    """NeuralNetwork interface."""

    def train(self):
        """Train network on all available models."""
        models = db.list_models()
        if not models:

            return {"error": {"code": 1, "message": "Collection of models is empty."}}

        # Get only valid models.
        valid_models = map(lambda m: m.is_valid, models)
        if len(valid_models) < 2:
            return {"error": {"code": 2, "message": "Number of valid models less than 2 "
                                                    "(only {})".format(len(valid_models))}}

        # Generate dataset for all valid models.
        x_train, y_train, x_val, y_val, x_test, y_test =\
            generate_dataset_from_images(*[os.path.join(m.path, "rendered") for m in models])

        # FIXME: Run training network in thread to prevent freeze.
        net2.fit(x_train, y_train)

    def get(self, action):
        """Do action with neural network."""
        return self.train()


api.add_resource(Uploader, "/upload")
api.add_resource(Models, "/models")
api.add_resource(NeuralNetwork, "/nn/<action>")


'''



class NeuralNetwork(Resource):
    """NeuralNetwork interface."""

    def train(self):
        """Train network on all available models."""
        models = db.list_models()
        if not models:
            return {"error": {"error_code": 1}}  # collection is empty!

        X_train, y_train, X_val, y_val, X_test, y_test = generate_dataset_from_images(*[os.path.join(m.path, "rendered") for m in models])

        # Train the network
        nn = net2.fit(X_train, y_train)



    def get(self, action):
        """Do action with neural network."""
        self.train()

    def post(self, action):
        """..."""
        print action


class Status(Resource):
    """Current status of the recognition module."""

    @staticmethod
    def get():
        """Get current status."""
        return db.get_status()





api.add_resource(Models, "/models")
api.add_resource(Status, "/status")
api.add_resource(NeuralNetwork, "/nn/<action>")

'''


if __name__ == '__main__':
    app.run(host="0.0.0.0", debug=True)


# TODO:
# 1. Запустить main и обучить модель. Лучше, если это будет сразу на какой-нибудь VPS.
# 2.