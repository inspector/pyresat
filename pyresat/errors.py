# coding: utf-8


class APIError(Exception):
    pass


class DatabaseError(Exception):
    pass
