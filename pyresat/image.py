# coding: utf-8

import numpy as np

from scipy import misc


def process(image_fp, threshold=0.3, size=(28, 28)):
    """..."""
    image = misc.imread(image_fp)

    # If image is already processed, just break execution without exceptions.
    if image.shape == size:
        return

    # Converts color image to matrix of 0s and 1s.
    image = np.where(np.sum(image[..., :3], axis=2) > threshold*755, 1, 0)

    # Find borders of valuable part.
    (y_min, y_max), (x_min, x_max) = map(lambda x: (x[0], x[-1]), np.nonzero(image))

    # Find center and size of cropped image.
    center = ((y_min + y_max) / 2, (x_min + x_max) / 2)
    t_size = max((y_max - y_min), (x_max - x_min))

    # Crop image.
    image = image[center[0]-t_size/2:center[0]+t_size/2, center[1]-t_size/2:center[1]+t_size/2]

    # Save resized image.
    misc.imsave(image_fp, misc.imresize(image, size))


if __name__ == "__main__":
    process("./storage/6b046d8c42d7145a7e5f203ca811635e/rendered/0_0_0.png")
