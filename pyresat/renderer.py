# coding: utf-8

import sys

from itertools import product
from pygame.constants import *
from OpenGL.GLU import *
from scipy import misc
from objloader import *
#from image import crop

def crop(img):
    return img


def render(model, size, positions):
    """Renders model from different positions (rx,ry,rz)."""

    pygame.init()
    viewport = size
    hx = viewport[0]/2
    hy = viewport[1]/2
    srf = pygame.display.set_mode(viewport, OPENGL | DOUBLEBUF)

    glLightfv(GL_LIGHT0, GL_POSITION,  (-40, 200, 100, 0.0))
    glLightfv(GL_LIGHT0, GL_AMBIENT, (0.2, 0.2, 0.2, 1.0))
    glLightfv(GL_LIGHT0, GL_DIFFUSE, (0.5, 0.5, 0.5, 1.0))
    glEnable(GL_LIGHT0)
    glEnable(GL_LIGHTING)
    glEnable(GL_COLOR_MATERIAL)
    glEnable(GL_DEPTH_TEST)
    glShadeModel(GL_SMOOTH)          # most obj files expect to be smooth-shaded

    obj = OBJ(model, swapyz=True)

    clock = pygame.time.Clock()

    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    width, height = viewport
    gluPerspective(90.0, width/float(height), 1, 100.0)
    glEnable(GL_DEPTH_TEST)
    glMatrixMode(GL_MODELVIEW)

    # prepare environment
    path = os.path.join(os.path.dirname(model), "rendered")
    if not os.path.isdir(path):
        os.makedirs(path)

    tx, ty = (0,0); zpos = 25
    for position in positions:
        clock.tick(30)

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
        glLoadIdentity()

        # RENDER OBJECT
        glTranslate(tx/20., ty/20., - zpos)
        glRotate(position[0], 1, 0, 0)
        glRotate(position[1], 0, 1, 0)
        glRotate(position[2], 0, 0, 1)
        glCallList(obj.gl_list)

        pygame.display.flip()

        filename = os.path.join(path, ''.join(str(p).rjust(3,'0') for p in position) + '.png')
        pygame.image.save(srf, filename)

        if position != (0,0,0):
            misc.imsave(filename, crop(misc.imread(filename))) # save cropped


if __name__ == "__main__":
    if len(sys.argv) != 2:
        exit(1)

    render(sys.argv[1], size=(500, 500), positions=product(*[range(0,405,45)]*3))
