import numpy as np

from renderer import render
from itertools import product
from neural import generate_dataset_from_images, plot_confusion_matrix, load_dataset, net1, net2
from image import crop_images


if __name__ == '__main__':

    #'''

    # render model
    render(model='../data/models/aura/aura.obj',
           size=(500, 500),
           positions=product(*[range(0,405,45)]*3))

    # resize
    # for file in data/output/*/*; do convert $file -resize 28x28 $file; done
    #'''

    '''
    X_train, y_train, X_val, y_val, X_test, y_test =\
                        generate_dataset_from_images('data/output/aura',
                                                     'data/output/tdrs')

    # Train the network
    nn = net2.fit(X_train, y_train)
    '''

    # Test - ok!
    #plot_confusion_matrix(y_test, net2.predict(X_test))

    #crop_images('data/photos/aura', 'data/photos/tdrs')

    '''
    X_train, y_train, X_val, y_val, X_test, y_test =\
                        generate_dataset_from_images('data/output/aura',
                                                     'data/output/tdrs')

    # Train the network
    nn = net2.fit(X_train, y_train)

    # Test
    plot_confusion_matrix(y_test, net2.predict(X_test))


    X_1, y_1, _, _, X_2, y_2 = generate_dataset_from_images('data/photos/aura',
                                                            'data/photos/tdrs')

    X_real = np.concatenate((X_1, X_2))
    y_real = np.concatenate((y_1, y_2))

    # Test
    plot_confusion_matrix(y_real, net2.predict(X_real))
    '''
