# coding: utf-8

import os
import json

from sqlalchemy import Column, Integer, String, create_engine
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from utils import Singleton, SuperLock, classlock, create_folder
from errors import DatabaseError
from datetime import datetime

Base = declarative_base()


class Model(Base):
    __tablename__ = "models"

    id = Column(Integer(), primary_key=True)
    name = Column(String(255), nullable=False)
    path = Column(String(255), nullable=False)

    def to_dict(self):
        """Converts object to dict.

        Returns:
             dict.
        """
        d = {}
        for column in self.__table__.columns:
            value = getattr(self, column.name)
            if isinstance(value, datetime):
                d[column.name] = value.strftime("%Y-%m-%d %H:%M:%S")
            else:
                d[column.name] = value

        return d

    def to_json(self):
        """Converts object to JSON.

        Returns:
            JSON data.
        """
        return json.dumps(self.to_dict())

    def is_valid(self):
        """Returns true if model contain all necessary data."""
        if os.path.isdir(os.path.join(self.path, "rendered")):
            return False

        for i in range(10):
            for j in range(10):
                for k in range(10):
                    if not os.path.isfile(os.path.join(self.path, "rendered", "{}_{}_{}.png".format(i, j, k))):
                        return False

        return True

class Status(Base):
    __tablename__ = "status"

    id = Column(Integer(), primary_key=True)
    name = Column(String(255), nullable=False)


class Database(object):
    """Database.

    This class handles the creation of the database user for internal queue management. It also provides some functions
    for interacting with it.
    """
    __metaclass__ = Singleton

    def __init__(self, echo=False):
        self._lock = SuperLock()

        db_file = os.path.join(os.path.dirname(__file__), "db", "pyresat.db")
        if not os.path.exists(db_file):
            db_dir = os.path.dirname(db_file)
            if not os.path.exists(db_dir):
                try:
                    create_folder(folder=db_dir)
                except Exception as e:
                    raise DatabaseError("Unable to create database directory: {0}".format(e))

        self._connect_database("sqlite:///%s" % db_file)

        # Disable SQL logging. Turn it on for debugging.
        self.engine.echo = echo

        # Connection timeout.
        self.engine.pool_timeout = 60

        # Create schema.
        try:
            Base.metadata.create_all(self.engine)
        except SQLAlchemyError as e:
            raise DatabaseError("Unable to create or connect to database: {0}".format(e))

        # Get db session.
        self.Session = sessionmaker(bind=self.engine)

    def __del__(self):
        """Disconnects pool."""
        self.engine.dispose()

    def _connect_database(self, connection_string):
        """Connect to a Database.

        Args:
            connection_string: connection string specifying the database.

        """
        try:
            # TODO: this is quite ugly, should improve.
            if connection_string.startswith("sqlite"):
                # Using "check_same_thread" to disable sqlite safety check on multiple threads.
                self.engine = create_engine(connection_string, connect_args={"check_same_thread": False})
            elif connection_string.startswith("postgres"):
                # Disabling SSL mode to avoid some errors using sqlalchemy and multiprocesing.
                # See: http://www.postgresql.org/docs/9.0/static/libpq-ssl.html#LIBPQ-SSL-SSLMODE-STATEMENTS
                self.engine = create_engine(connection_string, connect_args={"sslmode": "disable"})
            else:
                self.engine = create_engine(connection_string)
        except ImportError as e:
            raise DatabaseError("unable to import {0}, install with {0}".format(e.message.split()[-1]))

    @classlock
    def drop(self):
        """Drop all tables."""
        try:
            Base.metadata.drop_all(self.engine)
        except SQLAlchemyError as e:
            raise DatabaseError("Unable to create or connect to database: {0}".format(e))

    @classlock
    def get_status(self):
        """Get status."""
        session = self.Session()

        if not session.query(Status).count():
            self.set_status("stand-by")

        last_status = session.query(Status).first().name
        session.close()

        return last_status

    @classlock
    def set_status(self, status):
        """Set status."""
        session = self.Session()

        session.add(Status(name=status))
        try:
            session.commit()
        except SQLAlchemyError as e:
            session.rollback()
            raise DatabaseError("Unable to get status: {}".format(e))
        finally:
            session.close()

    @classlock
    def list_models(self):
        """Get list of all models.

        Returns:
            list of models.
        """
        session = self.Session()

        try:
            return session.query(Model).all()
        except SQLAlchemyError:
            return []
        finally:
            session.close()

    @classlock
    def add_model(self, name, path):
        """Create new model.

        Args:
            name: name of model.
            path: path to root with model.
        """
        session = self.Session()
        model = Model(name=name,
                      path=path)

        session.add(model)

        try:
            session.commit()
        except SQLAlchemyError as e:
            print "error", e
            session.rollback()
        finally:
            session.close()

    @classlock
    def view_model(self, model_id):
        """Show model.

        Args:
            model_id: ID of the model to query.

        Returns:
            details of the model.
        """
        session = self.Session()
        try:
            model = session.query(Model).get(model_id)
        except SQLAlchemyError as e:
            return None
        else:
            if model:
                session.expunge(model)
            return model
        finally:
            session.close()



