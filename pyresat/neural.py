# coding: utf-8

import os
import numpy as np
import lasagne

from lasagne import layers
from lasagne.updates import nesterov_momentum
from lasagne.nonlinearities import softmax
from nolearn.lasagne import NeuralNet
from sklearn.utils import shuffle
from sklearn.preprocessing import binarize
from scipy import misc


def generate_dataset_from_images(*folders):
    """..."""
    raw_data = [None, np.array([], np.uint8)]

    for i, folder in enumerate(folders):
        for filename in os.listdir(folder):
            filename = os.path.join(folder, filename)
            if os.path.isdir(filename) or filename.startswith("."):
                continue

            img = misc.imread(filename)
            if img.dtype != np.uint8:
                continue

            # Flatten and binarize.
            img = binarize(img.reshape(1, 784), threshold=0.3, copy=False)

            # Append to raw_data.
            raw_data[1] = np.append(raw_data[1], [i])
            if raw_data[0] is None:
                raw_data[0] = img
            else:
                raw_data[0] = np.concatenate((raw_data[0], img))

    # 10% from sample size.
    l = int(len(raw_data[0]) * 0.1)

    # Shuffle source data.
    raw_data = shuffle(raw_data[0], raw_data[1], random_state=0)

    # Split raw data into train, val and test.
    train_data = (raw_data[0][2*l:], raw_data[1][2*l:])
    test_data = (raw_data[0][l:2*l], raw_data[1][l:2*l])
    val_data = (raw_data[0][:l], raw_data[1][:l])

    return (train_data[0].reshape((-1, 1, 28, 28)),
            train_data[1].astype(np.uint8),
            val_data[0].reshape((-1, 1, 28, 28)),
            val_data[1].astype(np.uint8),
            test_data[0].reshape((-1, 1, 28, 28)),
            test_data[1].astype(np.uint8))


net1 = NeuralNet(
    layers=[('input', layers.InputLayer),
            ('conv2d1', layers.Conv2DLayer),
            ('maxpool1', layers.MaxPool2DLayer),
            ('conv2d2', layers.Conv2DLayer),
            ('maxpool2', layers.MaxPool2DLayer),
            ('dropout1', layers.DropoutLayer),
            ('dense', layers.DenseLayer),
            ('dropout2', layers.DropoutLayer),
            ('output', layers.DenseLayer),
            ],
    # input layer
    input_shape=(None, 1, 28, 28),
    # layer conv2d1
    conv2d1_num_filters=32,
    conv2d1_filter_size=(5, 5),
    conv2d1_nonlinearity=lasagne.nonlinearities.rectify,
    conv2d1_W=lasagne.init.GlorotUniform(),
    # layer maxpool1
    maxpool1_pool_size=(2, 2),
    # layer conv2d2
    conv2d2_num_filters=32,
    conv2d2_filter_size=(5, 5),
    conv2d2_nonlinearity=lasagne.nonlinearities.rectify,
    # layer maxpool2
    maxpool2_pool_size=(2, 2),
    # dropout1
    dropout1_p=0.5,
    # dense
    dense_num_units=256,
    dense_nonlinearity=lasagne.nonlinearities.rectify,
    # dropout2
    dropout2_p=0.5,
    # output
    output_nonlinearity=lasagne.nonlinearities.softmax,
    output_num_units=2,
    # optimization method params
    update=nesterov_momentum,
    update_learning_rate=0.01,
    update_momentum=0.9,
    max_epochs=50,
    verbose=1,
    )

net2 = NeuralNet(
    layers=[
        ('input', layers.InputLayer),
        ('hidden', layers.DenseLayer),
        ('hidden2', layers.DenseLayer),
        ('output', layers.DenseLayer),
        ],
    # layers parameters:
    input_shape=(None, 1, 28, 28),
    hidden_num_units=100,
    hidden2_num_units=100,
    output_nonlinearity=softmax,
    output_num_units=2,
    # optimization parameters:
    update=nesterov_momentum,
    update_learning_rate=0.01,
    update_momentum=0.9,
    regression=False,
    max_epochs=50,
    verbose=1,
    )
